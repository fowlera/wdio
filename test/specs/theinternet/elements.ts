import LandingPage from '../../pageobjects/landing.page';
import ElementsPage from '../../pageobjects/elements.page';

describe('The Internet', () => {
    it('should contain an Add Elements page', async () => {
        await LandingPage.waitUntilLoaded();
        await LandingPage.linkElements.click();
        await ElementsPage.waitUntilLoaded();
    });

    it('should allow you to add elements', async () => {
        await LandingPage.waitUntilLoaded();
        await LandingPage.linkElements.click();
        await ElementsPage.buttonAddElement.click();
        await expect(ElementsPage.buttonDeleteElement).toBeDisplayed();
    });

    it('should allow you to delete elements', async () => {
        await LandingPage.waitUntilLoaded();
        await LandingPage.linkElements.click();
        await ElementsPage.buttonAddElement.click();
        await ElementsPage.buttonDeleteElement.click();
        await expect(ElementsPage.buttonDeleteElement).not.toBeDisplayed();
    });
});
