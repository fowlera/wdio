import { ChainablePromiseElement } from 'webdriverio';

import Page from './page';

class ElementsPage extends Page {
    public get heading(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $('//h3[contains(text(),"Add/Remove Elements")]');
    }

    public get buttonAddElement(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $('//button[contains(text(),"Add Element")]');
    }

    public get buttonDeleteElement(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $('//button[contains(text(),"Delete")]');
    }

    public async isLoaded(): Promise<boolean> {
        return this.heading.isDisplayed();
    }

    public async waitUntilLoaded() {
        await browser.waitUntil(async () => {
            return (await this.heading).isDisplayed();
        });
    }
}

export default new ElementsPage();
