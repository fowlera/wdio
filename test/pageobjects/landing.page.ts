import { ChainablePromiseElement } from 'webdriverio';

import Page from './page';

class LandingPage extends Page {
    public get heading(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $('//h1[contains(text(),"Welcome to the-internet")]');
    }

    public get linkElements(): ChainablePromiseElement<Promise<WebdriverIO.Element>> {
        return $('//a[contains(@href,"add_remove_elements")]');
    }

    public async isLoaded(): Promise<boolean> {
        return this.heading.isDisplayed();
    }

    public async waitUntilLoaded() {
        await browser.waitUntil(async () => {
            return (await this.heading).isDisplayed();
        });
    }
}

export default new LandingPage();
