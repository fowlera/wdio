# Bootstrap testbed for WebdriverIO

This is a framework bootstrap, integrated with BrowserStack with an example test against https://the-internet.herokuapp.com/.

## Setup

Running `yarn` from this directory should install everything needed.

## Prerequisites

- Node installed
- A personal Browserstack account under the Safeguard org (contact me if you don't have one)

## To execute the example test

### Browserstack

Set the environment variables

BROWSERSTACK_USERNAME

BROWSERSTACK_ACCESS_KEY

which you can get from your Browserstack account.  If you don't have one, please get in touch. 

Run `yarn wdio:browserstack:theinternet` from this directory.

Navigate to the [Browserstack dashbaord](https://automate.browserstack.com/dashboard/v2/) to see the results of the test.

### Local

Running `yarn wdio:local:theinternet` from this directory should spawn multiple browsers and run the tests.

Note: There is a version coupling between the chromedriver package and the version of Chrome on your machine, so if you see widespread failures that normally means you need to update.

## To add your own tests for theinternet

E.g. if you wanted to add a login test for https://the-internet.herokuapp.com/login, the steps would be

- Add a new 'linkForms()' element for the 'Forms authentication' link, to ./test/pageobjects/landing.page.ts
- Create a new 'forms.ts' test file under ./test/specs (you can copy/paste from elements.ts to get the basic structure)
- Create a new 'forms.page.ts' model file under ./test/pageobjects (you can copy/paste from landing.page.ts to get the basic structure)
- Write the new forms pageobject, following the existing pattern (e.g. for the username, password, submit button etc)
- Write the new test in forms.ts, following the existing pattern (e.g. click the link, type in text, hit submit)

## To add your own tests for a different site

Once you've written some tests against theinternet, if you want to try another site, you'll just need to

- Create a new config file for it (e.g. create ./config/browserstack.google.conf.ts)
- Create the model and test files following the same pattern as above
- Add a new script to the package.json (e.g. `"wdio:browserstack:google": "wdio run conf/browserstack.google.conf.ts",`)
- And run with `yarn wdio:browserstack:google`

## Additional Docs

WDIO reference: https://webdriver.io/docs/gettingstarted/
