const localTheInternetConfig = require('./base.conf').config;

localTheInternetConfig.specs = [
    './test/specs/theinternet/elements.ts',
];

localTheInternetConfig.baseUrl = 'https://the-internet.herokuapp.com/',

module.exports.config = localTheInternetConfig;
