const browserstackTheInternetConfig = require('./browserstack.conf').config;

browserstackTheInternetConfig.specs = [
    './test/specs/theinternet/elements.ts',
];

browserstackTheInternetConfig.baseUrl = 'https://the-internet.herokuapp.com/',

module.exports.config = browserstackTheInternetConfig;
