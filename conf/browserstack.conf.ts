const browserstackConfig = require('./base.conf').config;

// ====================
// BROWSERSTACK CONFIG
// Doc: https://webdriver.io/docs/browserstack-service/
// ====================
browserstackConfig.user = process.env.BROWSERSTACK_USERNAME;
browserstackConfig.key = process.env.BROWSERSTACK_ACCESS_KEY;

browserstackConfig.services = [
  ['browserstack', { browserstackLocal: true }],
];

browserstackConfig.specs = [
  './test/specs/login/*.ts',
];

browserstackConfig.afterTest = async (test, context, { error, result, duration, passed, retries }) => {
  if (passed) {
    // Any pass specific functionality here
  } else {
    console.log();
    console.log(`## Test: ${test.parent}.${test.title}`);
    console.log(`## Error: ${error}`);
    console.log(`## Report: https://automate.browserstack.com/dashboard/v2/builds/fc87a1e17e87bc987b498fb96ed3176ebb9d3fc1/sessions/${browser.sessionId}`);
    console.log();
  }
};

module.exports.config = browserstackConfig;
